<?php

namespace Modules\UserManagement\Events;

use Carbon\Carbon;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Logout;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Request;
use Modules\UserManagement\Entities\LoginInfo;
use Modules\UserManagement\Repositories\LoginInfoRepository;

class AuthLoginEventHandler
{
    private $repository;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(LoginInfoRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Login $user
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function onUserLogin(Login $user)
    {
        $this->repository->create([
            'user_id' => $user->user->id,
            'login' => Carbon::now(),
            'ip' => Request::getClientIp(),
            'user_agent' =>  isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'none'
        ]);

        Cache::forget('user:'.$user->user->id.':history');
    }

    /**
     * @param Logout $user
     */
    public function onUserLogout(Logout $user)
    {
        $login = LoginInfo::latest()->where('user_id', '=', $user->user->id)->first();

        if ($login != null) {
            $login->logout = Carbon::now();

            $login->save();

            Cache::forget('user:'.$user->user->id.':history');
        }
    }
}
