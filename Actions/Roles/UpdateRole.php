<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/26/2019
 * Time: 6:59 PM
 */

namespace Modules\UserManagement\Actions\Roles;


use Illuminate\Support\Str;
use Modules\UserManagement\Repositories\RoleRepository;

class UpdateRole
{
    private $repository;

    public function __construct(RoleRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     * @param $id
     * @return mixed
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function execute(array $data, $id) {
        $this->repository->update([
            'name' => Str::slug($data['name'], '_'),
            'title' => $data['name'],
        ], $id);

        $role = $this->repository->find($id);

        $role->disallow($role->abilities);
        $role->allow($data['Permission']);

        return $role;
    }
}