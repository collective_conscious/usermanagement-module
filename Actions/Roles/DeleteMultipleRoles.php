<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/26/2019
 * Time: 6:59 PM
 */

namespace Modules\UserManagement\Actions\Roles;


use Illuminate\Support\Str;
use Modules\UserManagement\Repositories\RoleRepository;

class DeleteMultipleRoles
{
    private $repository;

    public function __construct(RoleRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $allChecked
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function execute(array $allChecked) {
        foreach ($allChecked as $id) {
            $model = $this->repository->find($id);

            $model->disallow($model->abilities);

            $this->repository->delete($id);
        }
    }
}