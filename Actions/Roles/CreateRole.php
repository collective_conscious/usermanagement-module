<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/26/2019
 * Time: 6:59 PM
 */

namespace Modules\UserManagement\Actions\Roles;


use Illuminate\Support\Str;
use Modules\UserManagement\Repositories\RoleRepository;

class CreateRole
{
    private $repository;

    public function __construct(RoleRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function execute(array $data) {
        $role = $this->repository->create([
            'name' => Str::slug($data['name'], '_'),
            'title' => $data['name'],
        ]);

        $role->allow($data['Permission']);

        return $role;
    }
}