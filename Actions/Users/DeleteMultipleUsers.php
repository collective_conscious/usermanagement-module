<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/26/2019
 * Time: 6:59 PM
 */

namespace Modules\UserManagement\Actions\Users;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Modules\UserManagement\Repositories\UserRepository;

class DeleteMultipleUsers
{
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $allChecked
     * @return array
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function execute(array $allChecked) {
        foreach ($allChecked as $id) {
            if($id == Auth::id()) {
                return [
                    'success' => false,
                    'code' => 400,
                    'msg' => 'You cannot delete yourself.'
                ];
            }

            $model = $this->repository->find($id);

            if(count($model->roles) > 0)
                $model->retract($model->roles);

            $this->repository->delete($id);

            Cache::forget('user:'.$id.':roles');
        }

        return [
            'success' => true,
            'code' => 200,
            'msg' => 'All Deleted'
        ];
    }
}