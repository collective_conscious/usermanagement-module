<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/26/2019
 * Time: 6:59 PM
 */

namespace Modules\UserManagement\Actions\Users;


use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Modules\UserManagement\Repositories\UserRepository;

class CreateUser
{
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function execute(array $data) {
        $user = $this->repository->create([
            'name' => $data['name'],
            'email' => $data['email'],
            'username' => $data['username'],
            'password' => bcrypt($data['password']),
            'epassword' => encrypt($data['password']),
            'status' => 1,
        ]);

        if(Arr::has($data, 'Role') && count($data['Role']) > 0)
            $user->assign($data['Role']);

        return $user;
    }
}