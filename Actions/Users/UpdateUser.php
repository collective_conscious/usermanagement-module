<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/26/2019
 * Time: 6:59 PM
 */

namespace Modules\UserManagement\Actions\Users;


use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Modules\UserManagement\Repositories\UserRepository;

class UpdateUser
{
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     * @param $id
     * @return mixed
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function execute(array $data, $id) {
        $user = $this->repository->find($id);

        $password = $user->password;
        $epassword = $user->epassword;

        if($data['password'] !== null && $data['password'] !== '') {
            $password = bcrypt($data['password']);
            $epassword = encrypt($data['password']);
        }

        $this->repository->update([
            'name' => $data['name'],
            'email' => $data['email'],
            'username' => $data['username'],
            'password' => $password,
            'epassword' =>$epassword,
            'status' => 1,
        ], $id);

        if(count($user->roles) > 0)
            $user->retract($user->roles);

        if(Arr::has($data, 'Role') && count($data['Role']) > 0)
            $user->assign($data['Role']);

        return $user;
    }
}