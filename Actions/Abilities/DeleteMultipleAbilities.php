<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/26/2019
 * Time: 6:59 PM
 */

namespace Modules\UserManagement\Actions\Abilities;


use Illuminate\Support\Str;
use Modules\UserManagement\Repositories\AbilityRepository;

class DeleteMultipleAbilities
{
    private $repository;

    public function __construct(AbilityRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $allChecked
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function execute(array $allChecked) {
        foreach ($allChecked as $id) {
            $this->repository->delete($id);
        }
    }
}