<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/26/2019
 * Time: 6:59 PM
 */

namespace Modules\UserManagement\Actions\Abilities;


use Illuminate\Support\Str;
use Modules\UserManagement\Repositories\AbilityRepository;

class CreateAbility
{
    private $repository;

    public function __construct(AbilityRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function execute(array $data) {
        return $this->repository->create([
            'name' => Str::slug($data['name'], '_'),
            'title' => $data['name'],
            'module' => $data['module']
        ]);
    }
}