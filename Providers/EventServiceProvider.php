<?php

namespace Modules\UserManagement\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Illuminate\Auth\Events\Login' => [
            'Modules\UserManagement\Events\AuthLoginEventHandler@onUserLogin',
        ],

        'Illuminate\Auth\Events\Logout' => [
            'Modules\UserManagement\Events\AuthLoginEventHandler@onUserLogout',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
