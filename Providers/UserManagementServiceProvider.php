<?php

namespace Modules\UserManagement\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Bouncer;

class UserManagementServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        if ($this->runningInConsole()) {
            $this->publishResources();
        }

        Bouncer::cache();

        view()->composer(['layout.app', 'layout.app_ajax'], function($view) {

            $id = Auth::id();

            $user = Cache::rememberForever('user:'.$id, function () {
                return Auth::user();
            });

            $user_roles = Cache::rememberForever('user:'.$id.':roles', function () use ($user) {
                return $user->roles;
            });

            $view->with('user', $user)
                ->with('user_roles', $user_roles);
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->app->register(EventServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('usermanagement.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'usermanagement'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/usermanagement');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/usermanagement';
        }, \Config::get('view.paths')), [$sourcePath]), 'usermanagement');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/usermanagement');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'usermanagement');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'usermanagement');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Publish the package's js, css files.
     *
     * @return void
     */
    protected function publishResources()
    {
        $stub = __DIR__.'/../Resources/assets/js/';

        $target = public_path('/app/js/user_management');

        $this->publishes([$stub => $target], 'resources');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    /**
     * Determine if we are running in the console.
     *
     * Copied from Laravel's Application class, since we need to support 5.1.
     *
     * @return bool
     */
    protected function runningInConsole()
    {
        return php_sapi_name() == 'cli' || php_sapi_name() == 'phpdbg';
    }
}
