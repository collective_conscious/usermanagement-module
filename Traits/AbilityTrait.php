<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/26/2019
 * Time: 6:27 PM
 */

namespace Modules\UserManagement\Traits;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Modules\UserManagement\Repositories\AbilityRepository;

trait AbilityTrait
{
    /**
     * @param AbilityRepository $abilityRepository
     * @return mixed
     */
    public function getAbilitiesByUserRole(AbilityRepository $abilityRepository) {
        if(Auth::user()->isA('applord')) {
            $abilities = $abilityRepository->all();
        }
        else {
            $abilities = Auth::user()->getAbilities();
        }

        return $abilities;
    }

    /**
     * @param AbilityRepository $abilityRepository
     * @return array
     */
    public function getAbilitySectionsArray(AbilityRepository $abilityRepository){
        $abilities = $this->getAbilitiesByUserRole($abilityRepository);

        $sections = array();

        foreach ($abilities as $ability){

            if(!Arr::has($sections, $ability->module))
                $sections[$ability->module] = $ability->module;
        }

        return $sections;
    }

    public function getModulesArray(AbilityRepository $abilityRepository) {
        $abilities = $abilityRepository->all();

        $Sections = array();

        foreach ($abilities as $permission){
            $module = Str::slug($permission->module, '_');

            if (!Arr::has($Sections, $module))
                $Sections[$module] = ucwords(str_replace('_', ' ', $permission->module));
        }

        return Arr::sort($Sections);
    }

    public function getTenantApplicationModulesArray() {
        $Sections = array();
        $Sections['user_management'] = 'User Management';
        $Sections['activity'] = 'Activity Logging';
        $Sections['setting'] = 'Setting';
        $Sections['auto_backup'] = 'Auto Backup';
        $Sections['accounting'] = 'Accounting';
        $Sections['other'] = 'Other';

        return Arr::sort($Sections);
    }

}