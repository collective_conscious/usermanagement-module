<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/26/2019
 * Time: 6:27 PM
 */

namespace Modules\UserManagement\Traits;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Modules\UserManagement\Repositories\Criteria\NotApplordRoleCriteria;
use Modules\UserManagement\Repositories\RoleRepository;

trait RoleTrait
{
    /**
     * @param RoleRepository $roleRepository
     * @return mixed
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function getRoleByUserRole(RoleRepository $roleRepository) {
        if(Auth::user()->isA('applord')) {
            $roles = $roleRepository->all();
        }
        else {
            $criteria = new NotApplordRoleCriteria();
            $roles = collect($roleRepository->getByCriteria($criteria)->all());
        }

        return $roles;
    }

}