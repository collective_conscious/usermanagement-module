<?php

namespace Modules\UserManagement\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Silber\Bouncer\Database\Role;
use Bouncer;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('roles')->truncate();
        DB::table('permissions')->truncate();

        Bouncer::refresh();

        $role = Role::create( ['name'=>'applord', 'title'=>'Applord'] );

        $role->allow([ 'View_permission', 'Assign_permission', 'Add_permission', 'Edit_permission', 'Delete_permission', 'Get_permission', 'Print_permission',
                                'View_role', 'Add_role', 'Edit_role', 'Delete_role', 'Get_role', 'Print_role',
                                'View_users', 'Add_users', 'Edit_users', 'Delete_users', 'Get_users', 'Print_users', 'Edit_Profile',
                                'View_Tenant', 'Add_Tenant', 'Edit_Tenant', 'Delete_Tenant', 'Get_Tenant', 'Print_Tenant',
                                'View_Subscription', 'Add_Subscription', 'Edit_Subscription', 'Delete_Subscription', 'Get_Subscription', 'Print_Subscription',
                                'View_login_info', 'Get_login_info', 'Delete_login_info', 'Print_login_info',]);

        Bouncer::refresh();

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
