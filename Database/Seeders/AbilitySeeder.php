<?php

namespace Modules\UserManagement\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Silber\Bouncer\Database\Ability;
use Bouncer;

class AbilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('abilities')->truncate();

        Bouncer::refresh();

        Ability::create( [ 'name'=>'View_Applord_Dashboard', 'module'=>'dashboard'] );
        Ability::create( [ 'name'=>'View_Staff_Dashboard', 'module'=>'dashboard'] );

        Ability::create( [ 'name'=>'View_permission', 'module'=>'permission'] );
        Ability::create( [ 'name'=>'Assign_permission', 'module'=>'permission'] );
        Ability::create( [ 'name'=>'Add_permission', 'module'=>'permission'] );
        Ability::create( [ 'name'=>'Edit_permission', 'module'=>'permission'] );
        Ability::create( [ 'name'=>'Get_permission', 'module'=>'permission'] );
        Ability::create( [ 'name'=>'Delete_permission', 'module'=>'permission'] );
        Ability::create( [ 'name'=>'Print_permission', 'module'=>'permission'] );

        Ability::create( [ 'name'=>'View_role', 'module'=>'role'] );
        Ability::create( [ 'name'=>'Add_role', 'module'=>'role'] );
        Ability::create( [ 'name'=>'Edit_role', 'module'=>'role'] );
        Ability::create( [ 'name'=>'Get_role', 'module'=>'role'] );
        Ability::create( [ 'name'=>'Delete_role', 'module'=>'role'] );
        Ability::create( [ 'name'=>'Print_role', 'module'=>'role'] );

        Ability::create( [ 'name'=>'View_Tenant', 'module'=>'tenant'] );
        Ability::create( [ 'name'=>'Add_Tenant', 'module'=>'tenant'] );
        Ability::create( [ 'name'=>'Edit_Tenant', 'module'=>'tenant'] );
        Ability::create( [ 'name'=>'Get_Tenant', 'module'=>'tenant'] );
        Ability::create( [ 'name'=>'Delete_Tenant', 'module'=>'tenant'] );
        Ability::create( [ 'name'=>'Print_Tenant', 'module'=>'tenant'] );

        Ability::create( [ 'name'=>'View_Subscription', 'module'=>'subscription'] );
        Ability::create( [ 'name'=>'Add_Subscription', 'module'=>'subscription'] );
        Ability::create( [ 'name'=>'Edit_Subscription', 'module'=>'subscription'] );
        Ability::create( [ 'name'=>'Get_Subscription', 'module'=>'subscription'] );
        Ability::create( [ 'name'=>'Delete_Subscription', 'module'=>'subscription'] );
        Ability::create( [ 'name'=>'Print_Subscription', 'module'=>'subscription'] );

        Ability::create( [ 'name'=>'View_users', 'module'=>'users'] );
        Ability::create( [ 'name'=>'Add_users', 'module'=>'users'] );
        Ability::create( [ 'name'=>'Edit_users', 'module'=>'users'] );
        Ability::create( [ 'name'=>'Get_users', 'module'=>'users'] );
        Ability::create( [ 'name'=>'Delete_users', 'module'=>'users'] );
        Ability::create( [ 'name'=>'Print_users', 'module'=>'users'] );
        Ability::create( [ 'name'=>'Edit_Profile', 'module'=>'users'] );

        Ability::create( [ 'name'=>'View_login_info', 'module'=>'login_info'] );
        Ability::create( [ 'name'=>'Get_login_info', 'module'=>'login_info'] );
        Ability::create( [ 'name'=>'Delete_login_info', 'module'=>'login_info'] );
        Ability::create( [ 'name'=>'Print_login_info', 'module'=>'login_info'] );

        Ability::create( [ 'name'=>'View_Setting', 'module'=>'setting'] );
        Ability::create( [ 'name'=>'Add_Setting', 'module'=>'setting'] );
        Ability::create( [ 'name'=>'Edit_Setting', 'module'=>'setting'] );
        Ability::create( [ 'name'=>'Get_Setting', 'module'=>'setting'] );
        Ability::create( [ 'name'=>'Delete_Setting', 'module'=>'setting'] );
        Ability::create( [ 'name'=>'Print_Setting', 'module'=>'setting'] );

        Ability::create( [ 'name'=>'View_Transaction', 'module'=>'accounts'] );
        Ability::create( [ 'name'=>'Add_Transaction', 'module'=>'accounts'] );
        Ability::create( [ 'name'=>'Edit_Transaction', 'module'=>'accounts'] );
        Ability::create( [ 'name'=>'Get_Transaction', 'module'=>'accounts'] );
        Ability::create( [ 'name'=>'Delete_Transaction', 'module'=>'accounts'] );
        Ability::create( [ 'name'=>'Print_Transaction', 'module'=>'accounts'] );
        Ability::create( [ 'name'=>'Approve_Transaction', 'module'=>'accounts'] );

        Ability::create( [ 'name'=>'View_Transaction_Head', 'module'=>'accounts'] );
        Ability::create( [ 'name'=>'Add_Transaction_Head', 'module'=>'accounts'] );
        Ability::create( [ 'name'=>'Edit_Transaction_Head', 'module'=>'accounts'] );
        Ability::create( [ 'name'=>'Get_Transaction_Head', 'module'=>'accounts'] );
        Ability::create( [ 'name'=>'Delete_Transaction_Head', 'module'=>'accounts'] );
        Ability::create( [ 'name'=>'Print_Transaction_Head', 'module'=>'accounts'] );

        Ability::create( [ 'name'=>'My_Activity', 'module'=>'activity'] );
        Ability::create( [ 'name'=>'All_Activity', 'module'=>'activity'] );
        Ability::create( [ 'name'=>'Print_Activity', 'module'=>'activity'] );

        Ability::create( [ 'name'=>'Download_Backup', 'module'=>'setting'] );
        Ability::create( [ 'name'=>'Automatic_Backup', 'module'=>'auto_backup'] );

        Ability::create( [ 'name'=>'View_Subscription_Plan', 'module'=>'subscription'] );
        Ability::create( [ 'name'=>'Add_Subscription_Plan', 'module'=>'subscription'] );
        Ability::create( [ 'name'=>'Edit_Subscription_Plan', 'module'=>'subscription'] );
        Ability::create( [ 'name'=>'Get_Subscription_Plan', 'module'=>'subscription'] );
        Ability::create( [ 'name'=>'Delete_Subscription_Plan', 'module'=>'subscription'] );
        Ability::create( [ 'name'=>'Print_Subscription_Plan', 'module'=>'subscription'] );

        Bouncer::refresh();

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
