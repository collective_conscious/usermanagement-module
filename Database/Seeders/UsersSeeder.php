<?php

namespace Modules\UserManagement\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Bouncer;
use Modules\UserManagement\Repositories\UserRepository;

class UsersSeeder extends Seeder
{
    private $userRepository;

    function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('users')->truncate();
        DB::table('assigned_roles')->truncate();

        Bouncer::refresh();

        $user = $this->userRepository->create([
            'name'=>'Applord',
            'email'=>'applord@app.com',
            'username'=>'applord',
            'password'=>bcrypt('applord'),
            'epassword'=>encrypt('applord'),
            'status'=>1
        ]);

        $user->assign('applord');

        Bouncer::refresh();

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
