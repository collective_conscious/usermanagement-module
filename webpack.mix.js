const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public/app').mergeManifest();

mix.js(__dirname + '/Resources/assets/js/assign_permission.js', 'js/user_management/assign_permission.js')
   .js(__dirname + '/Resources/assets/js/assign_role.js', 'js/user_management/assign_role.js')
   .js(__dirname + '/Resources/assets/js/history.js', 'js/user_management/history.js')
   .js(__dirname + '/Resources/assets/js/permission.js', 'js/user_management/permission.js')
   .js(__dirname + '/Resources/assets/js/role.js', 'js/user_management/role.js')
   .js(__dirname + '/Resources/assets/js/users.js', 'js/user_management/users.js');

if (mix.inProduction()) {
    mix.version();
}