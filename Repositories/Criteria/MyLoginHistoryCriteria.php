<?php
/**
 * Created by PhpStorm.
 * User: Bilal Younas
 * Date: 10/26/2019
 * Time: 6:11 PM
 */

namespace Modules\UserManagement\Repositories\Criteria;


use CollectiveConscious\RepositoryDesignPattern\Contracts\CriteriaInterface;
use CollectiveConscious\RepositoryDesignPattern\Contracts\RepositoryInterface;
use Illuminate\Support\Facades\Auth;

class MyLoginHistoryCriteria implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository) {
        return $model->where('user_id', '=', Auth::id());
    }
}