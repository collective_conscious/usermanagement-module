<?php

namespace Modules\UserManagement\Repositories;

use CollectiveConscious\RepositoryDesignPattern\Repository;
use Modules\UserManagement\Entities\User;


class UserRepository extends Repository {
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model() {
        return User::class;
    }
}