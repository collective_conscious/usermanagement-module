<?php

namespace Modules\UserManagement\Repositories;

use CollectiveConscious\RepositoryDesignPattern\Repository;
use Modules\UserManagement\Entities\LoginInfo;


class LoginInfoRepository extends Repository {
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model() {
        return LoginInfo::class;
    }
}