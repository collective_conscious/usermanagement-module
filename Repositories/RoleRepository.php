<?php

namespace Modules\UserManagement\Repositories;

use CollectiveConscious\RepositoryDesignPattern\Repository;
use Silber\Bouncer\Database\Role;

class RoleRepository extends Repository
{
    public function model()
    {
        return Role::class;
    }
}