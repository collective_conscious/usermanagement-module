<?php

namespace Modules\UserManagement\Repositories;

use CollectiveConscious\RepositoryDesignPattern\Repository;
use Silber\Bouncer\Database\Ability;

class AbilityRepository extends Repository
{
    public function model()
    {
        return Ability::class;
    }
}