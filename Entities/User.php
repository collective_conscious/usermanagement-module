<?php

namespace Modules\UserManagement\Entities;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Spatie\Activitylog\Traits\CausesActivity;
use Spatie\Activitylog\Traits\LogsActivity;


class User extends Authenticatable
{
    /*
     * If Activity Log Module is not added, Please remove LogsActivity and CausesActivity
     * Also remove the protected attributes $logName, $logAttributes, $logOnlyDirty
     */

    /*
     * To add Activity Module, update composer.json
     * add this package
     *
     * collective_conscious/activity-module
     *
     */

    use Notifiable,
        HasRolesAndAbilities,
        LogsActivity,  /* Remove in case, No Activity Module */
        CausesActivity;/* Remove in case, No Activity Module */

    protected static $logName = 'user'; /* Remove in case, No Activity Module */
    protected static $logAttributes = ['*'];/* Remove in case, No Activity Module */
    protected static $logOnlyDirty = true;/* Remove in case, No Activity Module */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'epassword', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function login_history() {
        return $this->hasMany(LoginInfo::class, 'user_id');
    }

    public function scopeLoginHistory() {
        return $this->with('login_history');
    }
}

