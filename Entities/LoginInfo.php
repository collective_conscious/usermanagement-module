<?php

namespace Modules\UserManagement\Entities;

use Illuminate\Database\Eloquent\Model;

class LoginInfo extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'ip', 'login', 'logout', 'user_agent'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
}
