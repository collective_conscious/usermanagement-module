<?php

namespace Modules\UserManagement\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;
use Bouncer;
use Modules\UserManagement\Entities\LoginInfo;
use Modules\UserManagement\Repositories\Criteria\MyLoginHistoryCriteria;
use Modules\UserManagement\Repositories\Criteria\UserLoginHistoryCriteria;
use Modules\UserManagement\Repositories\LoginInfoRepository;

class LoginHistoryController extends Controller
{
    public $repository;

    /**
     * Create a new controller instance.
     * @param LoginInfoRepository $repository
     */
    public function __construct(LoginInfoRepository $repository)
    {
        $this->middleware(['auth']);
        $this->repository = $repository;
    }

    /**
     * Show the application dashboard.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index($id, Request $request)
    {
        $id = base64_decode(urldecode($id));

        if ($request->ajax()) {
            return response()->json([
                'success' => true,
                'code' => 200,
                'view' => view('usermanagement::login_history.content', compact('id'))->render(),
            ]);
        }
        else
            return view('usermanagement::login_history.index', compact('id'));
    }

    /**
     * Send records to client based on ajax for long records
     *
     * @param $id
     * @param Request $request
     * @return void
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function history_ajax(Request $request)
    {
        $columns = array(
            0 =>'login',
            1=> 'logout',
            2=> 'user_agent',
            3=> 'ip',
        );

        $criteria = new UserLoginHistoryCriteria($request);
        $items = collect($this->repository->getByCriteria($criteria)->all());

        $totalData = $items->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            if($request->input('order.0.column') == 0 || $request->input('order.0.column') == 1)
                $collection = $items->slice($start, $limit)->sortByDate($order, ($dir === 'asc')?true:false);
            else
                $collection = $items->slice($start, $limit)->sortBy($order, ($dir === 'asc')?true:false);
        }
        else {
            $search = $request->input('search.value');

            if($request->input('order.0.column') == 0 || $request->input('order.0.column') == 1) {
                $collection = $items->filter(function ($item) use ($search) {
                    if (stripos($item->login, $search) !== false || stripos($item->logout, $search) !== false ||
                        stripos($item->user_agent, $search) !== false || stripos($item->ip, $search) !== false)
                        return $item;
                })
                    ->slice($start, $limit)
                    ->sortByDate($order, ($dir === 'asc')?true:false);
            }
            else {
                $collection = $items->filter(function ($item) use ($search) {
                    if (stripos($item->login, $search) !== false || stripos($item->logout, $search) !== false ||
                        stripos($item->user_agent, $search) !== false || stripos($item->ip, $search) !== false)
                        return $item;
                })
                    ->slice($start, $limit)
                    ->sortBy($order, ($dir === 'asc')?true:false);
            }


            $totalFiltered = $items->filter(function ($item) use ($search) {
                if (stripos($item->login, $search) !== false || stripos($item->logout, $search) !== false ||
                    stripos($item->user_agent, $search) !== false || stripos($item->ip, $search) !== false)
                    return $item;
            })
                ->count();
        }

        $data = array();
        if(!empty($collection))
        {
            $i = $start + 1;

            foreach ($collection as $item)
            {
                $nestedData['login'] = ($item->login != null) ? Carbon::parse($item->login)->diffForHumans() : '-';
                $nestedData['logout'] = ($item->logout != null) ? Carbon::parse($item->logout)->diffForHumans() : '-';
                $nestedData['user_agent'] = $item->user_agent;
                $nestedData['ip'] = $item->ip;

                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }
}
