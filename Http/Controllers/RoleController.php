<?php

namespace Modules\UserManagement\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Modules\UserManagement\Actions\Roles\CreateRole;
use Modules\UserManagement\Actions\Roles\DeleteMultipleRoles;
use Modules\UserManagement\Actions\Roles\UpdateRole;
use Modules\UserManagement\Http\Requests\RoleRequest;
use Modules\UserManagement\Repositories\AbilityRepository;
use Modules\UserManagement\Repositories\RoleRepository;
use Modules\UserManagement\Traits\AbilityTrait;
use Modules\UserManagement\Traits\RoleTrait;
use Silber\Bouncer\Database\Role;
use App\Http\Controllers\Controller;
use Bouncer;

class RoleController extends Controller
{
    use AbilityTrait, RoleTrait;

    private $repository;
    private $abilityRepository;

    /**
     * Create a new controller instance.
     *
     * @param RoleRepository $roleRepository
     * @param AbilityRepository $abilityRepository
     */
    public function __construct(RoleRepository $roleRepository, AbilityRepository $abilityRepository)
    {
        $this->middleware(['auth']);
        $this->repository = $roleRepository;
        $this->abilityRepository = $abilityRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return response()->json([
                'success' => true,
                'code' => 200,
                'view' => view('usermanagement::role.content')->render(),
            ]);
        }
        else
            return view('usermanagement::role.index');
    }

    /**
     * Send records to client based on ajax for long records
     *
     * @param Request $request
     * @return void
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function roles_ajax(Request $request)
    {
        $columns = array(
            0 =>'id',
            1=> 'name',
            2=> 'title',
            3=> 'created_at',
        );

        $roles = $this->getRoleByUserRole($this->repository);

        $totalData = $roles->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            if($request->input('order.0.column') == 3)
                $collection = $roles->slice($start, $limit)->sortByDate($order, ($dir === 'asc')?true:false);
            else
                $collection = $roles->slice($start, $limit)->sortBy($order, ($dir === 'asc')?true:false);
        }
        else {
            $search = $request->input('search.value');

            if($request->input('order.0.column') == 3) {
                $collection = $roles->filter(function ($item) use ($search) {
                        if (stripos($item->name, $search) !== false || stripos($item->title, $search) !== false)
                            return $item;
                    })
                    ->slice($start, $limit)
                    ->sortByDate($order, ($dir === 'asc')?true:false);
            }
            else {
                $collection = $roles->filter(function ($item) use ($search) {
                        if (stripos($item->name, $search) !== false || stripos($item->title, $search) !== false)
                            return $item;
                    })
                    ->slice($start, $limit)
                    ->sortBy($order, ($dir === 'asc')?true:false);
            }


            $totalFiltered = $roles->filter(function ($item) use ($search) {
                    if(stripos($item->name, $search) !== false || stripos($item->title, $search) !== false)
                        return $item;
                })
                ->count();
        }

        $data = array();
        if(!empty($collection))
        {
            $i = $start + 1;

            foreach ($collection as $item)
            {
                $nestedData['check'] = '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            <input type="checkbox" value="'.$item->id.'" class="checkboxes" data-set="#sample_1 .checkboxes" />
                                            <span></span>
                                        </label>';

                $action = null;

                if(Auth::user()->can('Edit_role')) {
                    $action .= '<a class="btn btn-icon-only blue popovers" data-container="body" data-trigger="hover" data-placement="auto"
                                    data-content="Edit Role detail." data-original-title="Edit Role"
                                    data-toggle="modal" data-target="#InputModal" data-modaltitle="Edit Role" data-modaltype="edit" data-company="'. ((Auth::user()->company == null)?null:((Auth::user()->company == null)?Auth::user()->company:null)) .'" data-record="'.$item->id.'" data-layouttoadd="roles/'.$item->id.'/edit" data-module="role" data-btnconfirm="Edit Role"
                                >
                                    <i class="fa fa-edit"></i>
                                </a>';
                }


                $nestedData['name'] = $item->title;
                $nestedData['coding_name'] = $item->name;
                $nestedData['added'] = Carbon::parse($item->created_at)->diffForHumans();
                $nestedData['action'] = $action;

                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function create() {

        $Permissions = $abilities = $this->getAbilitiesByUserRole($this->abilityRepository);
        $Sections = $this->getAbilitySectionsArray($this->abilityRepository);

        return response()->json([
            'success' => true,
            'code' => 200,
            'view' => view('usermanagement::role.create', compact('Permissions', 'Sections'))->render()
        ]);
    }

    public function store(RoleRequest $request, CreateRole $action)
    {
        $roleName = $request->name;

        $checkRole = $this->repository->all()->filter(function($record) use($roleName) {
            if($record->name == $roleName) {
                return $record;
            }
        });

        if(count($checkRole) > 0) {
            return response()->json([
                'success' => false,
                'code' => 400,
                'msg' => 'Role already exists.',
            ], 422);
        }

        $action->execute($request->all());

        Cache::forget('role:all');
        Bouncer::refresh();

        return response()->json([
            'success' => true,
            'code' => 200,
            'msg' => 'Role has been added.',
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        try {
            $role = $this->repository->find($id);
        }
        catch (ModelNotFoundException $exception) {
            return response()->json([
                'success' => false,
                'code' => 400,
                'msg' => 'Role cannot be updated. Please try again!',
            ], 422);
        }

        $Permissions = $abilities = $this->getAbilitiesByUserRole($this->abilityRepository);
        $Sections = $this->getAbilitySectionsArray($this->abilityRepository);

        return response()->json([
            'success' => true,
            'code' => 200,
            'view' => view('usermanagement::role.edit', compact('role', 'Permissions', 'Sections'))->render()
        ]);
    }

    public function update($id, RoleRequest $request, UpdateRole $action)
    {
        try {
            $role = $this->repository->find($id);
        } catch (QueryException $exception) {
            return response()->json([
                'success' => false,
                'code' => 400,
                'msg' => 'Role not found.',
            ], 422);
        }

        $RoleName = $request->name;

        $CheckRole = Role::all()->filter(function ($record) use ($id, $RoleName) {
            if ($id != $record->id) {
                if ($record->name == $RoleName) {
                    return $record;
                }
            }
        });

        if (count($CheckRole) > 0) {
            return response()->json([
                'success' => false,
                'code' => 400,
                'msg' => 'Role already exists.',
            ], 422);
        }

        $action->execute($request->all(), $id);

        Cache::forget('role:all');
        Bouncer::refresh();

        return response()->json([
            'success' => true,
            'code' => 200,
            'msg' => 'Role has been updated.',
        ]);
    }

    /**
     * Destroy the given permission.
     *
     * @param Request $request
     * @param DeleteMultipleRoles $action
     * @return Response
     */
    public function destroy(Request $request, DeleteMultipleRoles $action)
    {
        if ($request->get('AllChecked') != null){

            $allChecked = $request->get('AllChecked');

            try {
                $action->execute($allChecked);

                Cache::forget('role:all');

                return response()->json([
                    'success' => true,
                    'msg' => 'All selected roles have been deleted.'
                ]);
            }
            catch (\Exception $exception) {
                return response()->json([
                    'success' => false,
                    'code' => 400,
                    'msg' => $exception->getMessage(),
                ], 422);
            }
        }else {
            return response()->json([
                'success' => false,
                'code' => 400,
                'msg' => 'You have to select some item from table showing below.'
            ], 422);
        }
    }
}
