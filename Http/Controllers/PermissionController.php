<?php

namespace Modules\UserManagement\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Bouncer;
use Illuminate\Support\Facades\Cache;
use Modules\UserManagement\Actions\Abilities\CreateAbility;
use Modules\UserManagement\Actions\Abilities\DeleteMultipleAbilities;
use Modules\UserManagement\Actions\Abilities\UpdateAbility;
use Modules\UserManagement\Http\Requests\AbilityRequest;
use Modules\UserManagement\Repositories\AbilityRepository;
use Modules\UserManagement\Traits\AbilityTrait;
use Silber\Bouncer\Database\Ability;
use App\Http\Controllers\Controller;

class PermissionController extends Controller
{
    use AbilityTrait;

    private $repository;

    /**
     * Create a new controller instance.
     *
     * @param AbilityRepository $repository
     */
    public function __construct(AbilityRepository $repository)
    {
        $this->middleware(['auth']);
        $this->repository = $repository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return response()->json([
                'success' => true,
                'code' => 200,
                'view' => view('usermanagement::permission.content')->render(),
            ]);
        }
        else
            return view('usermanagement::permission.index');
    }

    /**
     * Send records to client based on ajax for long records
     *
     * @param Request $request
     * @return void
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function permission_ajax(Request $request)
    {
        $columns = array(
            0 =>'id',
            1 =>'module',
            2=> 'name',
            3=> 'title',
            4=> 'created_at',
        );

        $abilities = $this->getAbilitiesByUserRole($this->repository);

        $totalData = $abilities->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            if($request->input('order.0.column') == 4)
                $permissions = $abilities->slice($start, $limit)->sortByDate($order, ($dir === 'asc')?true:false);
            else
                $permissions = $abilities->slice($start, $limit)->sortBy($order, ($dir === 'asc')?true:false);
        }
        else {
            $search = $request->input('search.value');

            if($request->input('order.0.column') == 4) {
                $permissions = $abilities->filter(function ($ability) use ($search) {
                        if (stripos($ability->name, $search) !== false || stripos($ability->title, $search) !== false)
                            return $ability;
                    })
                    ->slice($start, $limit)
                    ->sortByDate($order, ($dir === 'asc')?true:false);
            }
            else {
                $permissions = $abilities->filter(function ($ability) use ($search) {
                        if (stripos($ability->name, $search) !== false || stripos($ability->title, $search) !== false)
                            return $ability;
                    })
                    ->slice($start, $limit)
                    ->sortBy($order, ($dir === 'asc')?true:false);
            }


            $totalFiltered = $abilities->filter(function ($ability) use ($search) {
                    if(stripos($ability->name, $search) !== false || stripos($ability->title, $search) !== false)
                        return $ability;
                })
                ->count();
        }

        $data = array();
        if(!empty($permissions))
        {
            $i = $start + 1;

            foreach ($permissions as $permission)
            {
                $nestedData['check'] = '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            <input type="checkbox" value="'.$permission->id.'" class="checkboxes" data-set="#sample_1 .checkboxes" />
                                            <span></span>
                                        </label>';

                $action = '';

                if(Auth::user()->can('Edit_permission')) {
                    $action .= '<a class="btn btn-icon-only blue popovers" data-container="body" data-trigger="hover" data-placement="auto"
                                    data-content="Edit Permission detail. Make sure to update the code." data-original-title="Edit Permission"
                                    data-toggle="modal" data-target="#InputModal" data-modaltitle="Edit Permission" data-modaltype="edit" data-company="'. ((Auth::user()->company == null)?null:((Auth::user()->company == null)?Auth::user()->company:null)) .'" data-record="'.$permission->id.'" data-layouttoadd="permissions/'.$permission->id.'/edit" data-module="permission" data-btnconfirm="Edit Permission"
                                >
                                    <i class="fa fa-edit"></i>
                                </a>';
                }


                $nestedData['section'] = $permission->module;
                $nestedData['name'] = $permission->title;
                $nestedData['coding_name'] = $permission->name;
                $nestedData['added'] = Carbon::parse($permission->created_at)->diffForHumans();
                $nestedData['action'] = $action;

                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function create() {
        return response()->json([
            'success' => true,
            'code' => 200,
            'view' => view('usermanagement::permission.create')->render()
        ]);
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store(AbilityRequest $request, CreateAbility $action)
    {
        $permissionName = $request->name;

        $checkPermission = $this->repository->all()->filter(function($record) use($permissionName) {
            if($record->name == $permissionName) {
                return $record;
            }
        });

        if(count($checkPermission) > 0) {
            return response()->json([
                'success' => false,
                'code' => 400,
                'msg' => 'Permission already exists.',
            ], 422);
        }

        $action->execute($request->all());

        Cache::flush();
        Bouncer::refresh();

        return response()->json([
            'success' => true,
            'code' => 200,
            'msg' => 'Permission has been added. Make sure it has been coded.',
        ]);
    }

    public function edit($id)
    {
        $permission = $this->repository->find($id);

        return response()->json([
            'success' => true,
            'code' => 200,
            'view' => view('usermanagement::permission.edit', compact('permission'))->render()
        ]);
    }

    public function update($id, AbilityRequest $request, UpdateAbility $action)
    {
        $permissionName = $request->name;

        $checkPermission = $this->repository->all()->filter(function ($record) use ($id, $permissionName) {
            if ($id != $record->id) {
                if ($record->name == $permissionName) {
                    return $record;
                }
            }
        });

        if (count($checkPermission) > 0) {
            return response()->json([
                'success' => false,
                'code' => 400,
                'msg' => 'Permission already exists.',
            ], 422);
        }

        try {

            $permission = $this->repository->find($id);

        } catch (QueryException $exception) {
            return response()->json([
                'success' => false,
                'code' => 400,
                'msg' => 'Permission not found.',
            ], 400);
        }

        $action->execute($request->all(), $permission->id);

        Cache::flush();
        Bouncer::refresh();

        return response()->json([
            'success' => true,
            'code' => 200,
            'msg' => 'Permission has been updated. Make sure it has been coded.',
        ]);
    }

    /**
     * Destroy the given permission.
     *
     * @param Request $request
     * @param DeleteMultipleAbilities $action
     * @return Response
     */
    public function destroy(Request $request, DeleteMultipleAbilities $action)
    {
        if ($request->get('AllChecked') != null){

            $allChecked = $request->get('AllChecked');

            try {
                $action->execute($allChecked);

                Cache::flush();
                Bouncer::refresh();

                return response()->json([
                    'success' => true,
                    'msg' => 'All selected permissions have been deleted. Edit the code to avoid any trouble.'
                ]);
            }
            catch (\Exception $exception) {
                return response()->json([
                    'success' => false,
                    'code' => 400,
                    'msg' => $exception->getMessage(),
                ], 422);
            }
        }else {
            return response()->json([
                'success' => false,
                'code' => 400,
                'msg' => 'You have to select some item from table showing below.'
            ], 422);
        }
    }
}
