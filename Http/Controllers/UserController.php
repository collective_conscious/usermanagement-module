<?php

namespace Modules\UserManagement\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;
use Modules\UserManagement\Actions\Users\CreateUser;
use Modules\UserManagement\Actions\Users\DeleteMultipleUsers;
use Modules\UserManagement\Actions\Users\UpdateUser;
use Modules\UserManagement\Http\Requests\UserRequest;
use Modules\UserManagement\Repositories\RoleRepository;
use Modules\UserManagement\Repositories\UserRepository;
use Modules\UserManagement\Traits\UserTrait;
use Bouncer;

class UserController extends Controller
{
    use UserTrait;

    public $repository;
    public $roleRepository;

    /**
     * Create a new controller instance.
     *
     * @param UserRepository $userRepository
     * @param RoleRepository $roleRepository
     */
    public function __construct(UserRepository $userRepository, RoleRepository $roleRepository)
    {
        $this->middleware(['auth']);
        $this->repository = $userRepository;
        $this->roleRepository = $roleRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return response()->json([
                'success' => true,
                'code' => 200,
                'view' => view('usermanagement::users.content')->render(),
            ]);
        }
        else
            return view('usermanagement::users.index');
    }

    /**
     * Send records to client based on ajax for long records
     *
     * @param Request $request
     * @return void
     * @throws \CollectiveConscious\RepositoryDesignPattern\Exceptions\RepositoryException
     */
    public function users_ajax(Request $request)
    {
        $columns = array(
            0 =>'id',
            1=> 'name',
            2=> 'email',
            3=> 'username',
            5=> 'status',
            6=> 'added',
        );

        $users = $this->repository->all();

        $totalData = $users->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            if($request->input('order.0.column') == 6)
                $collection = $users->slice($start, $limit)->sortByDate($order, ($dir === 'asc')?true:false);
            else
                $collection = $users->slice($start, $limit)->sortBy($order, ($dir === 'asc')?true:false);
        }
        else {
            $search = $request->input('search.value');

            if($request->input('order.0.column') == 6) {
                $collection = $users->filter(function ($item) use ($search) {
                    if (stripos($item->name, $search) !== false || stripos($item->username, $search) !== false)
                        return $item;
                })
                    ->slice($start, $limit)
                    ->sortByDate($order, ($dir === 'asc')?true:false);
            }
            else {
                $collection = $users->filter(function ($item) use ($search) {
                    if (stripos($item->name, $search) !== false || stripos($item->username, $search) !== false)
                        return $item;
                })
                    ->slice($start, $limit)
                    ->sortBy($order, ($dir === 'asc')?true:false);
            }


            $totalFiltered = $users->filter(function ($item) use ($search) {
                if(stripos($item->name, $search) !== false || stripos($item->username, $search) !== false)
                    return $item;
            })
                ->count();
        }

        $data = array();
        if(!empty($collection))
        {
            $i = $start + 1;

            foreach ($collection as $item)
            {
                $nestedData['check'] = '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            <input type="checkbox" value="'.$item->id.'" class="checkboxes" data-set="#sample_1 .checkboxes" />
                                            <span></span>
                                        </label>';

                $action = null;

                if(Auth::user()->can('Edit_users')) {
                    $action .= '<a class="btn btn-icon-only blue popovers" data-container="body" data-trigger="hover" data-placement="auto"
                                    data-content="Edit User detail." data-original-title="Edit User"
                                    data-toggle="modal" data-target="#InputModal" data-modaltitle="Edit User" data-modaltype="edit" data-company="'. ( Auth::user()->company ?? null ) .'" data-record="'.$item->id.'" data-layouttoadd="users/'.$item->id.'/edit" data-module="user" data-btnconfirm="Edit User"
                                >
                                    <i class="fa fa-edit"></i>
                                </a>';
                }

                if(Auth::user()->can('View_login_info')) {
                    $action .= '<a class="btn btn-icon-only red popovers" data-container="body" data-trigger="hover" data-placement="auto"
                                    data-content="View login history of this user." data-original-title="Login History"
                                    href="'.url("users/".urlencode(base64_encode($item->id))."/history").'" target="_blank"
                                >
                                    <i class="fa fa-sign-in"></i>
                                </a>';
                }


                $nestedData['name'] = $item->name;
                $nestedData['email'] = $item->email ?? '-';
                $nestedData['username'] = $item->username;

                $password = 'Not Available';

                try {
                    $password = '<div class="no-padding no-margin">
                                        <span id="SHPwd_'.$item->id.'" style="font-weight: bold; margin-right: 5px; " class="hide">'.decrypt($item->epassword).'</span>
                                        <button class="btn btn-xs btn-info popovers" data-trigger="hover" data-placement="auto"
                                                data-original-title="Hide/Show Password" 
                                                onclick="javascript:ShowHidePassword(\''.$item->id.'\', $(this))">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                   </div>';
                }
                catch (\Exception $exception) {
                    $password = 'Not Available';
                }

                $nestedData['password'] = $password;
                $nestedData['roles'] = $item->roles->pluck('title');
                $nestedData['status'] = ($item->status == 1)?'Active':'InActive';
                $nestedData['added'] = Carbon::parse($item->created_at)->diffForHumans();
                $nestedData['action'] = $action;

                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        $Roles = $this->roleRepository->all();

        return response()->json([
            'success' => true,
            'code' => 200,
            'view' => view('usermanagement::users.create', compact('Roles'))->render()
        ]);
    }

    public function store(UserRequest $request, CreateUser $action)
    {
        $action->execute($request->all());

        Cache::forget('user:all');
        Bouncer::refresh();

        return response()->json([
            'success' => true,
            'code' => 200,
            'msg' => 'User has been added.',
        ]);
    }

    public function edit($id)
    {
        try {
            $user = $this->repository->find($id);
        }
        catch (ModelNotFoundException $exception) {
            return response()->json([
                'success' => false,
                'code' => 400,
                'msg' => 'User cannot be updated. Please try again!',
            ], 422);
        }

        $Roles = $this->roleRepository->all();

        return response()->json([
            'success' => true,
            'code' => 200,
            'view' => view('usermanagement::users.edit', compact('Roles', 'user'))->render()
        ]);
    }

    public function update($id, UserRequest $request, UpdateUser $action)
    {
        try {
            $user = $this->repository->find($id);
        } catch (QueryException $exception) {
            return response()->json([
                'success' => false,
                'code' => 400,
                'msg' => 'User cannot be updated. Please try again!',
            ], 422);
        }

        $action->execute($request->all(), $id);

        Cache::forget('user:'.$id.':roles');
        Cache::forget('user:all');
        Bouncer::refresh();

        return response()->json([
            'success' => true,
            'code' => 200,
            'msg' => 'User has been updated.',
        ]);
    }

    /**
     * Destroy the given permission.
     *
     * @param  Request $request
     * @param DeleteMultipleUsers $action
     * @return Response
     */
    public function destroy(Request $request, DeleteMultipleUsers $action)
    {
        if ($request->get('AllChecked') != null){

            $allChecked = $request->get('AllChecked');

            try {
                $result = $action->execute($allChecked);

                if($result['code'] == 200) {
                    Cache::forget('user:all');

                    return response()->json([
                        'success' => true,
                        'msg' => 'All selected users have been deleted.'
                    ]);
                }
                else
                    return response()->json([
                        'success' => false,
                        'code' => 400,
                        'msg' => $result['msg'],
                    ], 422);
            }
            catch (\Exception $exception) {
                return response()->json([
                    'success' => false,
                    'code' => 400,
                    'msg' => $exception->getMessage(),
                ], 422);
            }
        }else {
            return response()->json([
                'success' => false,
                'code' => 400,
                'msg' => 'You have to select some item from table showing below.'
            ], 422);
        }
    }
}
