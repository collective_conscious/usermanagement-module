<?php

namespace Modules\UserManagement\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'name' => 'required|string',
                        'username' => 'required|string|unique:users',
                        'email' => 'nullable|string|email|unique:users',
                        'password' => 'required|string|min:6|confirmed',
                    ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                        'name' => 'required|string',
                        'username' => 'required|string|unique:users,username,'.$this->id,
                        'email' => 'nullable|string|email|unique:users,email,'.$this->id,
                    ];
                }
            default:
                return [];
        }
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Full name of the user is required.',
            'name.min' => 'Name should be greater than 2 characters',
            'username.required' => 'Please select a unique username for this user.',
            'username.min' => 'username should be greater than 2 characters',
        ];
    }
}
