
<div class="row">
    <div class="col-md-12">
        @include('errors.list')
        @include('errors.ajax_list')
        <div class="portlet light bordered">
            <div class="portlet-body form">

                {!! Form::model($user, ['method' => 'PATCH', 'class' => '', 'id' => 'form_sample_2', 'action' => ['\Modules\UserManagement\Http\Controllers\UserController@update', $user->id]]) !!}
                @include('usermanagement::users._form', ['CheckedRoles' => $user->roles, 'password' => decrypt($user->epassword),])
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('app/js/user_management/assign_role.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $('input').addClass('edited');
</script>