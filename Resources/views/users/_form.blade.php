
<div class="form-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group form-md-line-input form-md-floating-label {{ ($errors->has('username') ? 'has-error' : '') }}">
                {!! Form::text('username', null, array(
                    'id' => 'username',
                    'pattern' => '[A-Z,a-z,0-9]{6,12}',
                    'minlength' => 6,
                    'maxlength' => 12,
                    'required' => 'required',
                    'class' => 'form-control'
                )) !!}
                <label for="username">User Name <span style="color: red">*</span></label>
                <span class="help-block">Username Should be Unique. It should be between 6-12 characters long. Only alphabet and no spaces.</span>
            </div>
        </div>
        <!--/span-->
        <div class="col-md-6">
            <div class="form-group form-md-line-input form-md-floating-label {{ ($errors->has('name') ? 'has-error' : '') }}">
                {!! Form::text('name', null, array(
                    'id' => 'full_name',
                    'required' => 'required',
                    'class' => 'form-control'
                )) !!}
                <label for="full_name">Name <span style="color: red">*</span></label>
                <span class="help-block">Full name of the user.</span>
            </div>
        </div>
        <!--/span-->
    </div>
    <div class="row">

        <div class="col-md-6">
            <div class="form-group form-md-line-input form-md-floating-label {{ ($errors->has('password') ? 'has-error' : '') }}">
                <input type="password" value="{{ $password }}" minlength="6" maxlength="12" name="password" id="password" @if($password == null)autocomplete="new-password"@endif class="form-control" required>
                <label for="password">Password <span style="color: red">*</span></label>
                <span class="help-block">It should be between 6-12 characters long.</span>
            </div>
        </div>


        <div class="col-md-6">
            <div class="form-group form-md-line-input form-md-floating-label {{ ($errors->has('password_confirmation') ? 'has-error' : '') }}">
                <input type="password" value="{{ $password }}" minlength="6" maxlength="12" name="password_confirmation" id="password_confirmation" @if($password == null)autocomplete="new-password"@endif class="form-control" required>
                <label for="password">Confirm Password <span style="color: red">*</span></label>
                <span class="help-block">It should be between 6-12 characters long.</span>
            </div>
        </div>
    </div>
    <div class="row">
        <!--/span-->
        <div class="col-md-6">
            <div class="form-group form-md-line-input form-md-floating-label {{ ($errors->has('email') ? 'has-error' : '') }}">
                {!! Form::email('email', null, array(
                    'id' => 'email',
                    'class' => 'form-control'
                )) !!}
                <label for="contact">Email </label>
                <span class="help-block">Optional</span>
            </div>
        </div>
        <!--/span-->
    </div>
</div>

@include('usermanagement::users._assignRoles')

<div class="form-actions">
    <div class="row">
        <div class="col-md-12">
            <button type="submit" id="btnConfirm" class="btn green">Save Changes</button>
            <button type="reset" data-dismiss="modal" class="btn">Close</button>
        </div>
    </div>
</div>