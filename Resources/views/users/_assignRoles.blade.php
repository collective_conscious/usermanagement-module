
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-key font-red"></i>
                            <span class="caption-subject font-red sbold uppercase">Assign Roles</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-1" style="text-align: center; vertical-align: middle; padding: 0.3%">
                                <div class="md-checkbox-inline">
                                    <div class="md-checkbox has-info">
                                        {!! Form::checkbox('AllCheck', 1, (count($Roles) == count($CheckedRoles))?true:false, array(
                                            'id' => 'AllCheck',
                                            'class' => 'md-check '
                                        )) !!}
                                        <label for="AllCheck">
                                            <span></span>
                                            <span class="check popovers" data-container="body" data-trigger="hover" data-placement="auto"
                                                  data-content="Un-assign All roles" data-original-title="Un-assign All selected roles and their permissions"></span>
                                            <span class="box popovers" data-container="body" data-trigger="hover" data-placement="auto"
                                                  data-content="Assign All Roles" data-original-title="Assign All Roles and their permissions"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <h4>Roles</h4>
                            </div>
                        </div>

                        <div class="row" style="padding: 1.7%;">
                            <div class="col-md-10">
                                <div class="md-checkbox-inline">
                                    @foreach($Roles as $role)
                                        <div class="md-checkbox has-success">
                                            {!! Form::checkbox('Role['.$role->id.']', $role->id, ($CheckedRoles != null)?(Arr::has($CheckedRoles->keyBy('id')->toArray(), $role->id))?true:false:false, array(
                                                'id' => $role->id,
                                                'class' => 'md-check roles'
                                            )) !!}
                                            <label for="{{ $role->id }}">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span> {{ $role->title }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>