
        <div class="form-body">

            {!! Form::hidden('company_id', session('active_company'), array(
                'id' => 'company_hidden',
                'class' => 'form-control'
            )) !!}

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group form-md-line-input form-md-floating-label {{ ($errors->has('name') ? 'has-error' : '') }}">
                        {!! Form::text('name', null, array(
                            'id' => 'name',
                            'required' => 'required',
                            'class' => 'form-control'
                        )) !!}
                        <label for="name">Name <span style="color: red">*</span></label>
                        <span class="help-block">Name to be used in coding</span>
                    </div>
                </div>
                <!--/span-->
            </div>
        </div>

        @include('usermanagement::permission._assignPermissions')

        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" id="btnConfirm" class="btn green">Save Changes</button>
                    <button type="reset" data-dismiss="modal" class="btn">Close</button>
                </div>
            </div>
        </div>