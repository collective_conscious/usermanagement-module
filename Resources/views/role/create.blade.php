
    <div class="row">
        <div class="col-md-12">
            @include('errors.list')
            @include('errors.ajax_list')
            <div class="portlet light bordered">
                <div class="portlet-body form">

                    {!! Form::open(['url' => 'roles', 'class' => '', 'id' => 'form_sample_2']) !!}
                        @include('usermanagement::role._form', ['CheckedPermissions' => []])
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('app/js/user_management/assign_permission.js') }}" type="text/javascript"></script>
