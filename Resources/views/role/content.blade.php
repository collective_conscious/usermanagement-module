

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ url('home') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Roles</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <div class="btn-group btn-group-solid">
                @if(Auth::user()->can('Add_role'))
                    <a  type="button" class="btn blue popovers" data-container="body" data-trigger="hover" data-placement="auto"
                        data-content="Superuser can add roles. Superuser should assign permissions to a role. A maximum permissions a role cana have." data-original-title="Add Role"
                        data-toggle="modal" data-target="#InputModal" data-modaltitle="Add Role" data-modaltype="add" data-company="{{ (Auth::user()->company == null)?null:Auth::user()->company }}" data-record="" data-layouttoadd="roles/create" data-module="role" data-btnconfirm="Add Role"
                    >
                        <i class="fa fa-plus"></i>
                        Add Role
                    </a>
                @endif
                @if(Auth::user()->can('Delete_role'))
                    <button type="button" onclick="javascript:Delete('roles', 'By deleting these roles, the user will not have any permissions to whom these roles are assigned.')" class="btn purple-studio popovers" data-container="body" data-trigger="hover" data-placement="auto"
                            data-content="Delete roles by selecting one or many from below." data-original-title="Delete Roles">
                        <i class="fa fa-trash"></i>
                        Delete Roles
                    </button>
                @endif
                <div class="btn-group pull-right">
                    <button class="btn red btn-outline dropdown-toggle popovers" data-container="body" data-trigger="hover" data-placement="auto"
                            data-content="You can print, copy or get a PDF, Excel or CSV of the data showing below." data-original-title="Tools" data-toggle="dropdown">
                        <i class="fa fa-share"></i>
                        <span class="hidden-xs"> Trigger Tools </span>
                        <i class="fa fa-angle-down"></i>
                    </button>
                    <ul class="dropdown-menu pull-right" id="sample_1_tools">
                        @if(Auth::user()->can('Print_role'))
                            <li>
                                <a href="javascript:;" data-action="0" class="tool-action">
                                    <i class="icon-printer"></i> Print</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="1" class="tool-action">
                                    <i class="icon-check"></i> Copy</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="2" class="tool-action">
                                    <i class="icon-doc"></i> PDF</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="3" class="tool-action">
                                    <i class="icon-paper-clip"></i> Excel</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="4" class="tool-action">
                                    <i class="icon-cloud-upload"></i> CSV</a>
                            </li>
                            <li class="divider"> </li>
                        @endif
                        <li>
                            <a href="javascript:;" data-action="5" class="tool-action">
                                <i class="icon-refresh"></i> Clear Filters</a>
                        </li>
                    </ul>
                </div>
                <button class="btn dark btn-outline popovers" id="sample_1_tools_1" data-action="6" data-container="body" data-trigger="hover" data-placement="auto"
                        data-content="You can view or hide columns and print or download desired output." data-original-title="Column Visibility">
                    Columns
                </button>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Roles
    <small>All the roles, this system supports are shown below.</small>
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->

<script type="text/javascript">
    LoadURL = '{{ url('roles_ajax') }}';
    ShowAction = {{ (Auth::user()->can(['Edit_role']))?'true':'false' }};
</script>

<div class="alert alert-success alert-dismissible hide" id="AjaxSuccessAlert">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span></span>
</div>

<div class="alert alert-danger alert-dismissible hide" id="AjaxMainErrorAlert">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span></span>
</div>

<div class="row">
    <div class="col-md-12">

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover table-checkable order-column sample_1" id="tblRole_{{ Auth::id() }}">
                    <thead>
                    <tr>
                        <th>
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                <input type="checkbox" class="group-checkable" data-set="#tblRole_{{ Auth::id() }} .checkboxes" />
                                <span></span>
                            </label>
                        </th>
                        <th> Name </th>
                        <th> Coding Name </th>
                        <th> Added </th>
                        @if(Auth::user()->can(['Edit_role']))
                            <th> Actions </th>
                        @endif
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>


<script src="{{ asset('app/js/user_management/role.js') }}" type="text/javascript"></script>