
    <div class="row">
        <div class="col-md-12">
            @include('errors.list')
            @include('errors.ajax_list')
            <div class="portlet light bordered">
                <div class="portlet-body form">

                    {!! Form::open(['url' => 'permissions', 'class' => '', 'id' => 'form_sample_2']) !!}
                        @include('usermanagement::permission._addPermission', [])
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
