
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-key font-red"></i>
                            <span class="caption-subject font-red sbold uppercase">Assign Permissions</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-1" style="text-align: center; vertical-align: middle; padding: 0.3%">
                                <div class="md-checkbox-inline">
                                    <div class="md-checkbox has-info">
                                        {!! Form::checkbox('AllCheck', 1, (count($Permissions) == count($CheckedPermissions))?true:false, array(
                                            'id' => 'AllCheck',
                                            'class' => 'md-check '
                                        )) !!}
                                        <label for="AllCheck">
                                            <span></span>
                                            <span class="check popovers" data-container="" data-trigger="hover" data-placement="auto"
                                                  data-content="Un-assign All permissions from all sections" data-original-title="Un-assign All Sections"></span>
                                            <span class="box popovers" data-container="" data-trigger="hover" data-placement="auto"
                                                  data-content="Assign All permissions from all sections" data-original-title="Assign All Sections"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <h4>Section</h4>
                            </div>
                            <div class="col-md-9">
                                <h4>Permissions</h4>
                            </div>
                        </div>
                        @foreach($Sections as $section)

                            @php
                                $TotalPermissionsInThisSection = count($Permissions->filter(function ($item, $key) use ($section) {
                                                                                            if($item->module == $section)
                                                                                                return $item;
                                                                                        }));

                                $TotalCheckedInThisSection = 0;

                                if($CheckedPermissions != null){
                                    $TotalCheckedInThisSection = count($CheckedPermissions->filter(function ($item, $key) use ($section) {
                                                                                                if($item->module == $section)
                                                                                                    return $item;
                                                                                            }));
                                }
                            @endphp

                            <hr>

                            @if($section == 'Permission')
                                <div class="row hide">
                            @else
                                <div class="row">
                            @endif

                                <div class="col-md-1" style="text-align: center; vertical-align: middle; padding: 0.3%">
                                    <div class="md-checkbox-inline">
                                        <div class="md-checkbox has-info">
                                            {!! Form::checkbox('Section['.$section.']', $section, ($TotalPermissionsInThisSection == $TotalCheckedInThisSection)?true:false, array(
                                                'id' => $section,
                                                'class' => 'md-check sections'
                                            )) !!}
                                            <label for="{{ $section }}">
                                                <span></span>
                                                <span class="check popovers" data-container="" data-trigger="hover" data-placement="auto"
                                                      data-content="Un-assign All permissions from this section" data-original-title="Un-assign This Section"></span>
                                                <span class="box popovers" data-container="" data-trigger="hover" data-placement="auto"
                                                      data-content="Assign All permissions from this section" data-original-title="Assign This Section"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <h5>{{ $section }}</h5>
                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="md-checkbox-inline">
                                                @foreach($Permissions as $permission)
                                                    @if($permission->module == $section)
                                                        <div class="md-checkbox has-success">
                                                            {!! Form::checkbox('Permission['.$permission->id.']', $permission->id, ($CheckedPermissions != null)?(Arr::has($CheckedPermissions->keyBy('id')->toArray(), $permission->id))?true:false:false, array(
                                                                'id' => $permission->id,
                                                                'class' => 'md-check permissions '.$section
                                                            )) !!}
                                                            <label for="{{ $permission->id }}">
                                                                <span></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> {{ $permission->title }}
                                                            </label>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>