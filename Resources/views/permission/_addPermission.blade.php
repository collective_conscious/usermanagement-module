
        <div class="form-body">
            <div class="row">

                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group form-md-line-input form-md-floating-label {{ ($errors->has('module') ? 'has-error' : '') }}">
                        {!! Form::text('module', null, array(
                            'id' => 'module',
                            'required' => 'required',
                            'class' => 'form-control'
                        )) !!}
                        <label for="module">Module <span style="color: red">*</span></label>
                        <span class="help-block">Module or Section this permission relates to... (No Spaces)</span>
                    </div>
                </div>
                <!--/span-->

                <div class="col-md-6">
                    <div class="form-group form-md-line-input form-md-floating-label {{ ($errors->has('name') ? 'has-error' : '') }}">
                        {!! Form::text('name', null, array(
                            'id' => 'name',
                            'required' => 'required',
                            'class' => 'form-control'
                        )) !!}
                        <label for="name">Name <span style="color: red">*</span></label>
                        <span class="help-block">Name to be used in coding</span>
                    </div>
                </div>
                <!--/span-->
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" id="btnConfirm" class="btn green">Save Changes</button>
                    <button type="reset" data-dismiss="modal" class="btn">Close</button>
                </div>
            </div>
        </div>