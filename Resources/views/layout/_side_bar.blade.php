@if($user->can('View_users') || $user->can('View_role') || $user->can('View_permission') || $user->can('View_Settings'))
    <li class="heading">
        <h3 class="uppercase">System</h3>
    </li>
@endif

@if($user->can('View_users'))
    <li class="navbar_ajax nav-item
                                            {{ (Request::is('users') ? 'active' : '') }}
    {{ ((Request::is('users/'.request()->route('id').'/history')) ? 'active' : '') }}
            " id="NavBar_users">
        <a href="javascript:LoadView('users', 'get', null, true);" class="nav-link ">
            <i class="icon-users"></i>
            <span class="title">Users</span>
            <span id="NavBar_arrow_users" class="navbar_arrow
                                                                                {{ ((Request::is('users')) ? 'selected' : '') }}
            {{ ((Request::is('users/'.request()->route('id').'/history')) ? 'selected' : '') }}
                    "
            ></span>
        </a>
    </li>
@endif

@if($user->can('View_role'))
    <li class="navbar_ajax nav-item {{ (Request::is('roles') ? 'active' : '') }}" id="NavBar_roles">
        <a href="javascript:LoadView('roles', 'get', null, true);" class="nav-link ">
            <i class="fa fa-magnet"></i>
            <span class="title">Roles</span>
            <span id="NavBar_arrow_roles" class="navbar_arrow {{ ((Request::is('roles')) ? 'selected' : '') }}"></span>
        </a>
    </li>
@endif

@if($user->can('View_permission'))
    <li class="navbar_ajax nav-item {{ (Request::is('permissions') ? 'active' : '') }}" id="NavBar_permissions">
        <a href="javascript:LoadView('permissions', 'get', null, true);" class="nav-link ">
            <i class="icon-key"></i>
            <span class="title">Permissions</span>
            <span id="NavBar_arrow_permissions" class="navbar_arrow {{ ((Request::is('permissions')) ? 'selected' : '') }}"></span>
        </a>
    </li>
@endif