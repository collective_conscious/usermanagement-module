var table = $('#tblUser_'+window.User);

DTMain = $('#tblUser_'+window.User).dataTable({

    // Internationalisation. For more info refer to http://datatables.net/manual/i18n
    "language": {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "No data available in table",
        "info": "Showing _START_ to _END_ of _TOTAL_ records",
        "infoEmpty": "No records found",
        "infoFiltered": "(filtered1 from _MAX_ total records)",
        "lengthMenu": "Show _MENU_",
        "search": "Search:",
        "zeroRecords": "No matching records found",
        "paginate": {
            "previous":"Prev",
            "next": "Next",
            "last": "Last",
            "first": "First"
        },
        "processing": '<i class="fa fa-spinner fa-spin fa-3x" style="color: #DE8E30"></i>\n'
    },

    "stateSave": true, // save datatable state(pagination, sort, etc) in cookie.
    "retrieve": true,
    "processing": true,
    "serverSide": true,
    "ajax": {
        "url": LoadURL,
        "dataType": "json",
        "type": "POST",
        "data":{ _token: window.Laravel.csrfToken},
        error: function(xhr, error, thrown) {
            console.log(xhr);
            swal({
                    title: "Something went wrong",
                    text: "Error has been logged and We are looking in to the issue. Your patience is appreciated.",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Refresh",
                    closeOnConfirm: true
                },
                function(){
                    DTMain.DataTable().ajax.reload();
                });
        }
    },
    "initComplete": function(settings, json) {
        $('.popovers').popover();
    },
    columns: [
        { "data": "check" },
        { "data": "name" },
        { "data": "email" },
        { "data": "username" },
        { "data": "password" },
        { "data": "roles" },
        { "data": "status" },
        { "data": "added" },
        { "data": "action" },
    ],

    "lengthMenu": [
        [10, 25, 50, 100],
        [10, 25, 50, 100] // change per page values here
    ],
    // set the initial value
    "pageLength": 25,
    "pagingType": "bootstrap_full_number",
    "columnDefs": [
        {  // set default column settings
            'orderable': false,
            'targets': [0, 4, 5, 8]
        },
        {
            "searchable": false,
            "targets": [0, 4, 5, 8]
        },
        {
            "className": "no-padding",
            "targets": [4]
        },
        {
            "visible": ShowAction,
            "targets": [8]
        }
    ],
    "order": [
        [1, "asc"]
    ], // set first column as a default sort by asc
    buttons: [
        {
            extend: 'print',
            className: 'btn default',
            header : true,
            footer: true,
            title: 'App_Users_'+Date.now(),
            exportOptions: {
                columns: ':visible'
            }
        },
        { extend: 'copy', className: 'btn default' },
        {
            extend: 'pdf',
            className: 'btn default',
            header : true,
            footer: true,
            title: 'App_Users_'+Date.now(),
            exportOptions: {
                columns: ':visible'
            }
        },
        {
            extend: 'excel',
            className: 'btn default',
            header : true,
            footer: true,
            title: 'App_Users_'+Date.now(),
            exportOptions: {
                columns: ':visible'
            }
        },
        {
            extend: 'csv',
            className: 'btn default',
            header : true,
            footer: true,
            title: 'App_Users_'+Date.now(),
            exportOptions: {
                columns: ':visible'
            }
        },
        {
            text: 'Clear Filters',
            className: 'btn default',
            action: function ( e, dt, node, config ) {
                this.state.clear();
                window.location.reload();
            }
        },
        {
            extend: 'colvis',
            text: 'Columns',
            collectionLayout: 'fixed two-column'
        }
    ]
});

$('#sample_1_tools > li > a.tool-action').on('click', function() {
    var action = $(this).attr('data-action');
    DTMain.DataTable().button(action).trigger();
});

$('#sample_1_tools_1').on('click', function() {
    var action = $(this).attr('data-action');
    DTMain.DataTable().button(action).trigger();
});

var tableWrapper = jQuery('#sample_1_wrapper');

table.find('.group-checkable').change(function () {
    var set = jQuery(this).attr("data-set");
    var checked = jQuery(this).is(":checked");
    jQuery(set).each(function () {
        if (checked) {
            $(this).prop("checked", true);
            $(this).parents('tr').addClass("active");
        } else {
            $(this).prop("checked", false);
            $(this).parents('tr').removeClass("active");
        }
    });
});

table.on('change', 'tbody tr .checkboxes', function () {
    $(this).parents('tr').toggleClass("active");

    if($(this).prop('checked')){

        $('.group-checkable').prop('checked', (($('.checkboxes:checked').length == $('.checkboxes').length)?true:false));

    }
    else {

        $('.group-checkable').prop('checked', (($('.checkboxes:checked').length == $('.checkboxes').length)?true:false));

    }
});

function ShowHidePassword(id, btn) {
    if($('#SHPwd_'+id).hasClass('hide')) {
        $('#SHPwd_' + id).removeClass('hide');
        btn.removeClass('btn-info').addClass('btn-danger');
        btn.find('i').removeClass('fa-eye').addClass('fa-eye-slash');
    }
    else {
        $('#SHPwd_' + id).addClass('hide');
        btn.removeClass('btn-danger').addClass('btn-info');
        btn.find('i').removeClass('fa-eye-slash').addClass('fa-eye');
    }
}