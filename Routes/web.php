<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
     * Starting Routes for UserController
     */

Route::group(['middleware' => ['can:View_users']], function () {
    Route::get('users', 'UserController@index')->name('users.index');
    Route::post('users_ajax', 'UserController@users_ajax')->name('users.users_ajax');
});

Route::group(['middleware' => ['can:Add_users']], function () {
    Route::get('users/create', 'UserController@create')->name('users.create');
    Route::post('users', 'UserController@store')->name('users.store');
});

Route::group(['middleware' => ['can:Edit_users']], function () {
    Route::get('users/{id}/edit', 'UserController@edit')->name('users.edit');
    Route::patch('users/{id}', 'UserController@update')->name('users.update');
});

Route::group(['middleware' => ['can:View_login_info']], function () {
    Route::get('users/{id}/history', 'LoginHistoryController@index')->name('users.history');
    Route::post('users/history_ajax', 'LoginHistoryController@history_ajax')->name('users.history_ajax');
});

Route::group(['middleware' => ['can:Delete_users']], function () {
    Route::delete('users', 'UserController@destroy')->name('users.destroy');
});

/*
 * Ending Routes for UserController
 */


/*
 * Starting Routes for RolesController
 */

Route::group(['middleware' => ['can:View_role']], function () {
    Route::get('roles', 'RoleController@index')->name('roles.index');
    Route::post('roles_ajax', 'RoleController@roles_ajax')->name('roles.roles_ajax');
});

Route::group(['middleware' => ['can:Add_role']], function () {
    Route::get('roles/create', 'RoleController@create')->name('roles.create');
    Route::post('roles', 'RoleController@store')->name('roles.store');
});

Route::group(['middleware' => ['can:Edit_role']], function () {
    Route::get('roles/{id}/edit', 'RoleController@edit')->name('roles.edit');
    Route::patch('roles/{id}', 'RoleController@update')->name('roles.update');
});

Route::group(['middleware' => ['can:Delete_role']], function () {
    Route::delete('roles', 'RoleController@destroy')->name('roles.destroy');
});

/*
 * Ending Routes for RolesController
 */

/*
 * Starting Routes for PermissionsController
 */

Route::group(['middleware' => ['can:View_permission']], function () {
    Route::get('permissions', 'PermissionController@index')->name('permissions.index');
    Route::post('/permission_ajax', 'PermissionController@permission_ajax')->name('permissions.permission_ajax');
});

Route::group(['middleware' => ['can:Add_permission']], function () {
    Route::get('permissions/create', 'PermissionController@create')->name('permissions.create');
    Route::post('permissions', 'PermissionController@store')->name('permissions.store');
});

Route::group(['middleware' => ['can:Edit_permission']], function () {
    Route::get('permissions/{id}/edit', 'PermissionController@edit')->name('permissions.edit');
    Route::patch('permissions/{id}', 'PermissionController@update')->name('permissions.update');
});

Route::group(['middleware' => ['can:Delete_permission']], function () {
    Route::delete('permissions', 'PermissionController@destroy')->name('permissions.destroy');
});

/*
 * Ending Routes for PermissionsController
 */
